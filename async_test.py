import asyncio

async def le_function(a,b,cal):
    a = float(a)
    b = float(b)
    if a == None or b == None or cal == None:
        return "One or more pars are none."

    result = eval("{}{}{}".format(a,cal,b))
    if str(result).split(".")[1] == "0":
        result = int(result)
    return result

while True:
    n1 = input("Num a: ")
    if n1 == "stop":
        break
    n2 = input("Num b: ")
    if n2 == "stop":
        break
    o = input("What would you like to do to these numbers? ")
    if o == "stop":
        break

    try:
        res = asyncio.run(le_function(n1,n2,o))
    except:
        print("You must input two numbers and an operator.")
    print("\nYour result is: ", res, "\n")
