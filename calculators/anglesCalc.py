#Script
while True:
    while True:
        try:
            sides = int(input("How many sides does the polygon have? "))
        except:
            print("Invalid input.")
            continue
        if sides < 3:
            print("Invalid input.")
            continue
        break

    intangle = ((sides - 2) * 180) / (sides)
    if str(intangle).split(".")[1] == "0":
        intangle = int(intangle)
    extangle = 180 - intangle
    sumangles = intangle * sides
    print(f"Interior angle: {intangle}°\nExterior angle: {extangle}°\nSum of interior angles: {sumangles}°\nSum of exterior angles: 360°")
