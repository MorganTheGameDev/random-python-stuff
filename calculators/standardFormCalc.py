#Script
while True:
    try:
        number = input("Input a number to convert ")
    except:
        print("Invalid input.")
        continue

    if "x10" in number:
        number = number.split("x10^")
        number[1] = int(number[1]) - 1
        for i in range(number[1]):
            number[1] = str(number[1])
            number[0] = float(number[0])
            if number[1].startswith("-"):
                number[0] /= 10
            else:
                number[0] *= 10
        print(int(number[0]))
    else:
        number = float(number)
        power = 0
        while True:
            if number >= 10:
                number /= 10
                power += 1
            elif number <1:
                number *= 10
                power -= 1
            else:
                number = int(number)
                number = f"{number}x10^{power}"
                break
        print(number)
    
