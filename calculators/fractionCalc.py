#Imports
from math import gcd

#Definitions
def addFractions(frac1, frac2):
    frac1 = frac1.split("/")
    frac2 = frac2.split("/")
    numerator = (int(frac1[0]) * int(frac2[1])) + (int(frac1[1]) * int(frac2[0]))
    denominator = int(frac1[1]) * int(frac2[1])
    numerator, denominator = int(numerator / gcd(numerator, denominator)), int(denominator / gcd(numerator, denominator))
    return f"{numerator}/{denominator}"

def subtractFractions(frac1, frac2):
    frac1 = frac1.split("/")
    frac2 = frac2.split("/")
    numerator = (int(frac1[0]) * int(frac2[1])) - (int(frac1[1]) * int(frac2[0]))
    denominator = int(frac1[1]) * int(frac2[1])
    numerator, denominator = int(numerator / gcd(numerator, denominator)), int(denominator / gcd(numerator, denominator))
    return f"{numerator}/{denominator}"

def multiplyFractions(frac1, frac2):
    frac1 = frac1.split("/")
    frac2 = frac2.split("/")
    numerator = int(frac1[0]) * int(frac2[0])
    denominator = int(frac1[1]) * int(frac2[1])
    numerator, denominator = int(numerator / gcd(numerator, denominator)), int(denominator / gcd(numerator, denominator))
    return f"{numerator}/{denominator}"

def divideFractions(frac1, frac2):
    frac1 = frac1.split("/")
    frac2 = frac2.split("/")
    numerator = int(frac1[0]) * int(frac2[1])
    denominator = int(frac1[1]) * int(frac2[0])
    numerator, denominator = int(numerator / gcd(numerator, denominator)), int(denominator / gcd(numerator, denominator))
    return f"{numerator}/{denominator}"

#Script
while True:
    while True:
        frac1 = input("Input first fraction ")
        if "/" in frac1:
            if len(frac1.split("/")) == 2:
                break
            else:
                print("Incorrect input.")
        else:
            print("Incorrect input.")

    while True:
        operand = input("Input operator (+, -, x or /) ")
        if operand in "+-x/":
            if len(operand) == 1:
                break
            else:
                print("Incorrect input.")
        else:
            print("Incorrect input.")

    while True:
        frac2 = input("Input second fraction ")
        if "/" in frac2:
            if len(frac2.split("/")) == 2:
                break
            else:
                print("Incorrect input.")
        else:
            print("Incorrect input.")

    if operand == "+":
        print(addFractions(frac1, frac2))
    elif operand == "-":
        print(subtractFractions(frac1, frac2))
    elif operand == "x":
        print(multiplyFractions(frac1, frac2))
    else:
        print(divideFractions(frac1, frac2))
        
    
