#Imports
from math import pi

#Script
while True:
    while True:
        try:
            radius = float(input("Input radius (cm) "))
        except:
            print("Invalid input.")
            continue
        break

    if str(radius).split(".")[1] == "0":
        radius = int(radius)
    area = pi * (radius**2)
    circumference = (pi * 2) * radius
    print(f"Area: {area}cm²\nCircumference: {circumference}cm")
